// Librairies
import React, { useState } from 'react';
// Composants
import { StyleSheet, View, Button, Image } from 'react-native';

export default function App() {
	// State
	const [nightMode, setNightMode] = useState(false);

	// Méthodes
	const switchMode = () => {
		setNightMode(!nightMode);
	};

	return (
		<View
			style={{
				...styles.container,
				backgroundColor: nightMode ? '#2c3e50' : '#ecf0f1',
			}}>
			{nightMode ? (
				<Image source={require('./assets/moon.png')} style={styles.logo} />
			) : (
				<Image source={require('./assets/sun.png')} style={styles.logo} />
			)}
			<Button
				title={nightMode ? 'Passer en mode clair' : 'passer en mode sombre'}
				onPress={switchMode}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	logo: {
		width: 150,
		height: 150,
		marginBottom: 30,
	},
});
